# Eigen Calc

[![pipeline status](https://gitlab.com/degv364/eigen_calc/badges/master/pipeline.svg)](https://gitlab.com/degv364/eigen_calc/commits/master)
[![coverage report](https://gitlab.com/degv364/eigen_calc/badges/master/coverage.svg)](https://gitlab.com/degv364/eigen_calc/commits/master)

This is just a little repo for playing around with a particular singular value decomposition
algorithm. I am trying to understand how orocos-KDL implemented SVD, so I took their code
and isolated it here to play around with it and hopefully improve it.

The original code of this SVD algorithm can be found [here](https://github.com/orocos/orocos_kinematics_dynamics)

## Dependencies (direct)

- gcovr
- Eigen
- gtest