#include "utilities.hpp"


void fill_jacobian(MatrixXd& jacobian) {
  double values [42] = {-0.330827, 0.261285, -0.0634603, -0.640334,
			-0.44059, -0.00594982, -0.20031,
			-1.48783, -1.80195, -0.39123, -0.911489,
			-0.717405, 0.675781, 0.0976329,
			-0.0176899, 0.160687, -0.0226411, -0.952894,
			-0.85285, 0.0807787, 0.0185394,
			-0.218351, 0.852363, -0.387827, 0.629791,
			0.632632, 0.971581, 0.133574,
			0.036957, 0.167762, 0.00956914, -0.727376,
			-0.721899, 0.0365101, 0.0865809,
			0.97517, 0.495311, 0.921682, 0.272557,
			0.280427, -0.233875, 0.98725};
  for (int i=0; i<jacobian.rows(); i++){
    for (int j=0; j<jacobian.cols(); j++) {
      jacobian(i, j) = values[j+i*jacobian.cols()];
    }
  }
}

void fill_random_jacobian(MatrixXd& jacobian) {
  jacobian = 10 * MatrixXd::Random(jacobian.rows(), jacobian.cols());
}


bool similar_matrix(MatrixXd& left, MatrixXd& right, double epsilon) {
  double error=0;
  int rows = left.rows();
  int cols = left.cols();
  if (rows != right.rows() || cols != right.cols()) return false;
  MatrixXd diff = left - right;
  for (int i=0; i<rows; i++){
    for (int j=0; j<cols; j++){
      error += fabs(diff(i, j));
    }
  }
  return (error < epsilon);
}

bool similar_vector(VectorXd& left, VectorXd& right, double epsilon) {
  double error=0;
  int size = left.rows();
  VectorXd diff(size);
  if (size != right.rows()) return false;
  diff = left-right;
  for (int i; i<size; i++){
    error += fabs(diff(i));
  }
  return (error < epsilon);
}


