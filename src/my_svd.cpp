// Copyright  (C)  2007  Ruben Smits <ruben dot smits at mech dot kuleuven dot be>

// Version: 1.0
// Author: Ruben Smits <ruben dot smits at mech dot kuleuven dot be>
// Maintainer: Ruben Smits <ruben dot smits at mech dot kuleuven dot be>
// URL: http://www.orocos.org/kdl

// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include "my_svd.hpp"

namespace NEW{
    /// Sort the eigen values from bigger to lower. And reorder the eigen
    /// vectors accordingly. This is a Bubble sort, which is OK because
    /// Jacobians will have at most 6 non-zero singular values
    void sort_eigen_values(MatrixXd& u_m, VectorXd& s_v, MatrixXd& v_m){
	int col, max_col, j;
	double max_singular, singular, swap_val;

	for (col=0; col<u_m.cols(); col++){
	    max_singular = s_v(col);
	    max_col = col;
	    for (j=col+1; j<u_m.cols(); j++){
		singular = s_v(j);
		if (singular > max_singular){
		    max_singular = singular;
		    max_col = j;
		}
	    }
	    if (max_col != col){
		// swap eigen values
		swap_val = s_v(col);
		s_v(col) = s_v(max_col);
		s_v(max_col) = swap_val;

		// swap eigen vectors
		u_m.col(col).swap(u_m.col(max_col));
		v_m.col(col).swap(v_m.col(max_col));
	    }
	}
    }

    int householder_reduction_to_bidiagonal(MatrixXd& U, VectorXd& S,
					    MatrixXd& V, VectorXd& tmp,
					    double* anorm, double epsilon) {
	double scale(0), g(0), s(0), f(0), h(0);
	int ppi;
	int rows = U.rows();
	int cols = U.cols();
	for (int i=0;i<cols;i++) {
	    ppi=i+1;
	    tmp(i)=scale*g;
	    g=s=scale=0.0;
	    if (i<rows) {
		// compute the sum of the i-th column, starting from the
		//i-th row
		for (int k=i;k<rows;k++){
		    scale += fabs(U(k,i));
		}
		if (fabs(scale)>epsilon) {
		    // multiply the i-th column by 1.0/scale, start from the
		    //i-th element
		    // sum of squares of column i, start from the i-th element
		    for (int k=i;k<rows;k++) {
			U(k,i) /= scale;
			s += U(k,i)*U(k,i);
		    }
		    f=U(i,i);  // f is the diag elem
		    if (s<0) return -3;
		    g = -SIGN(sqrt(s),f);
		    h=f*g-s;
		    U(i,i)=f-g;
		    for (int j=ppi;j<cols;j++) {
			// dot product of columns i and j,
			//starting from the i-th row
			s = 0.0;
			for (int k=i;k<rows;k++){
			    s += U(k,i)*U(k,j);
			}
			if (h==0) return -4;
			f=s/h;
			// copy the scaled i-th column into the j-th column
			for (int k=i;k<rows;k++){
			    U(k,j) += f*U(k,i);
			}
		    }
		    for (int k=i;k<rows;k++){
			U(k,i) *= scale;
		    }
		}
	    }
	    // save singular value
	    S(i)=scale*g;
	    g=s=scale=0.0;
	    if ((i <rows) && (i+1 != cols)) {
		// sum of row i, start from columns i+1
		for (int k=ppi;k<cols;k++){
		    scale += fabs(U(i,k));
		}
		if (fabs(scale)>epsilon) {
		    for (int k=ppi;k<cols;k++) {
			U(i,k) /= scale;
			s += U(i,k)*U(i,k);
		    }
		    f=U(i,ppi);
		    if (s<0) return -5;
		    g = -SIGN(sqrt(s),f);
		    h=f*g-s;
		    U(i,ppi)=f-g;
		    if (h==0) return -6;
		    for (int k=ppi;k<cols;k++){
			tmp(k)=U(i,k)/h;
		    }
		    for (int j=ppi;j<rows;j++) {
			s = 0.0;
			for (int k=ppi;k<cols;k++){
			    s += U(j,k)*U(i,k);
			}
			for (int k=ppi;k<cols;k++){
			    U(j,k) += s*tmp(k);
			}
		    }
		    for (int k=ppi;k<cols;k++){
			U(i,k) *= scale;
		    }
		}
	    }
	    *anorm = fabs(S(i))+fabs(tmp(i))!=0? 1: *anorm;
	}
	return 0;
    }

    int accumulate_right_hand_transformations(MatrixXd& U, VectorXd& S,
					      MatrixXd& V, VectorXd& tmp,
					      double epsilon) {
	double g, s;
	int cols=U.cols();
	int ppi = cols;
	for (int i=cols-1;i>=0;i--) {
	    if (i<cols-1) {
		if (fabs(g)>epsilon) {
		    if (U(i,ppi)==0) return -7;
		    for (int j=ppi;j<cols;j++){
			V(j,i)=(U(i,j)/U(i,ppi))/g;
		    }
		    for (int j=ppi;j<cols;j++) {
			s = 0.0;
			for (int k=ppi;k<cols;k++){
			    s += U(i,k)*V(k,j);
			}
			V.block(ppi,j,cols-ppi,1) += s*V.block(
			    ppi,i,cols-ppi,1);
		    }
		}
		V.block(i,ppi,1,cols-ppi) *= 0;
		V.block(ppi,i,cols-ppi,1) *= 0;
	    }
	    V(i,i)=1.0;
	    g=tmp(i);
	    ppi=i;
	}
	return 0;
    }

    int accumulate_left_hand_transformations(MatrixXd& U, VectorXd& S,
					     MatrixXd& V, VectorXd& tmp,
					     double epsilon) {
	int ppi;
	double f, g, s;
	const int cols = U.cols();
	const int rows = U.rows();
	for (int i=cols-1<rows-1 ? cols-1:rows-1;i>=0;i--) {
	    ppi=i+1;
	    g=S(i);
	    // Null block
	    U.block(i, ppi, 1, cols-ppi) *= 0.0;
	    if (fabs(g)>epsilon) {
		g=1.0/g;
		for (int j=ppi;j<cols;j++) {
		    s = 0.0;
		    for (int k=ppi;k<rows;k++){
			s += U(k,i)*U(k,j);
		    }
		    if (U(i,i)==0) return -8;
		    f=(s/U(i,i))*g;
		    U.block(i,j, rows-i, 1) += f*U.block(i,i, rows-i,1);
		}
		U.block(i,i,rows-i,1) *= g;
	    } else {
		U.block(i, i, rows-i, 1) *= 0.0;
	    }
	    ++U(i,i);
	}
	return 0;
    }

    int next_qr_transformation(MatrixXd& U, VectorXd& S, MatrixXd& V,
			       VectorXd& tmp, double* x_in, double* f_in,
			       int ppi, int nm, double epsilon) {
	double c, s, g, y, z, h;
	int i, j, jj;
	int rows = U.rows();
	int cols = U.cols();
	double x = *x_in;
	double f = *f_in;
	c=s=1.0;
	for (j=ppi;j<=nm;j++) {
	    i=j+1;
	    g=tmp(i);
	    y=S(i);
	    h=s*g;
	    g=c*g;
	    z=PYTHAG(f,h);
	    tmp(j)=z;
	    if (z==0) return -13;
	    c=f/z;
	    s=h/z;
	    f=x*c+g*s;
	    g=g*c-x*s;
	    h=y*s;
	    y=y*c;
	    for (jj=0;jj<cols;jj++) {
		x=V(jj,j);
		z=V(jj,i);
		V(jj,j)=x*c+z*s;
		V(jj,i)=z*c-x*s;
	    }
	    z=PYTHAG(f,h);
	    S(j)=z;
	    if (fabs(z)>epsilon) {
		z=1.0/z;
		c=f*z;
		s=h*z;
	    }
	    f=(c*g)+(s*y);
	    x=(c*y)-(s*g);
	    for (jj=0;jj<rows;jj++) {
		y=U(jj,j);
		z=U(jj,i);
		U(jj,j)=y*c+z*s;
		U(jj,i)=z*c-y*s;
	    }
	}
	*x_in = x;
	*f_in = f;
	return 0;
    }

    int diagonalize_bidiagonal_form(MatrixXd& U, VectorXd& S, MatrixXd& V,
				    VectorXd& tmp, int maxiter, double anorm,
				    double epsilon){
	double c(0),f(0),h(0),s(0),x(0),y(0),z(0),g(0);
	int nm=0;
	int ppi(0);
	bool flag;

	int rows = U.rows();
	int cols = U.cols();

	for (int k=cols-1;k>=0;k--) {  // Loop over singular values.
	    for (int its=1;its<=maxiter;its++) {// Loop over allowed iterations.
		flag=true;
		for (int i=k;i>=0;i--) {  // Test for splitting.
		    ppi = i;
		    if ((fabs(tmp(i))+anorm) == anorm) {
			flag=false;
			break;
		    }
		    nm=i-1;  // Note that tmp[1] is always zero.
		    if ((fabs(S(nm)+anorm) == anorm)) break;
		}
		if (flag) {
		    c=0.0;  // Cancellation of tmp[l], if l>1:
		    s=1.0;
		    for (int i=ppi;i<=k;i++) {
			f=s*tmp(i);
			tmp(i)=c*tmp(i);
			if ((fabs(f)+anorm) == anorm) break;
			g=S(i);
			h=PYTHAG(f,g);
			S(i)=h;
			if (h==0) return -9;
			h=1.0/h;
			c=g*h;
			s=(-f*h);
			for (int j=0;j<rows;j++) {
			    y=U(j,nm);
			    z=U(j,i);
			    U(j,nm)=y*c+z*s;
			    U(j,i)=z*c-y*s;
			}
		    }
		}
		z=S(k);

		if (ppi == k) {  // Convergence.
		    if (z < 0.0) {  // Singular value is made nonnegative.
			S(k) = -z;
			for (int j=0;j<cols;j++){
			    V(j,k)=-V(j,k);
			}
		    }
		    break;
		}
		x=S(ppi);  // Shift from bottom 2-by-2 minor:
		nm=k-1;
		y=S(nm);
		g=tmp(nm);
		h=tmp(k);
		if (h==0 || y==0) return -10;
		f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);

		g=PYTHAG(f,1.0);
		if (x==0) return -11;
		if ((f+SIGN(g,f))==0) return -12;
		f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;

		/* Next QR transformation: */
		int error = next_qr_transformation(
		    U, S, V, tmp, &x, &f, ppi, nm, epsilon);
		if (error != 0) return error;

		tmp(ppi)=0.0;
		tmp(k)=f;
		S(k)=x;
		if (its == maxiter) return (-2);
	    }
	}
	return 0;
    }

    int my_svd(const MatrixXd& A,MatrixXd& U,VectorXd& S,MatrixXd& V,
	       VectorXd& tmp,int maxiter,double epsilon){
	U.setZero();
	U.topLeftCorner(A.rows(),A.cols())=A;

	int error;
	double anorm(0);

	error = householder_reduction_to_bidiagonal(
	    U, S, V, tmp, &anorm, epsilon);
	if (error != 0) return error;
	
	error = accumulate_right_hand_transformations(
	    U, S, V, tmp, epsilon);
	if (error != 0) return error;
	
	error = accumulate_left_hand_transformations(
	    U, S, V, tmp, epsilon);
	if (error != 0) return error;

	/* Diagonalization of the bidiagonal form. */
	error = diagonalize_bidiagonal_form(
	    U, S, V, tmp, maxiter, anorm, epsilon);
	if (error != 0) return error;
	
	sort_eigen_values(U, S, V);

	return 0;
    }
}
