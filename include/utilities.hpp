#ifndef UTILITIES_HPP
#define UTILITIES_HPP
#include <Eigen/Core>

using namespace Eigen;

void fill_jacobian(MatrixXd& jacobian);
void fill_random_jacobian(MatrixXd& jacobian);
bool similar_matrix(MatrixXd& left, MatrixXd& right, double epsilon);
bool similar_vector(VectorXd& left, VectorXd& right, double epsilon);

#endif
