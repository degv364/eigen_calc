#include <gtest/gtest.h>
#include "svd_eigen_HH.hpp"
#include "my_svd.hpp"
#include "utilities.hpp"

TEST (SvdEigenHHTest, ReturnsZero) {
  int error;
  Eigen::MatrixXd jacobian(6, 7);
  Eigen::MatrixXd u_matrix(6, 7);
  Eigen::VectorXd s_vector(7);
  Eigen::MatrixXd v_matrix(7, 7);
  Eigen::VectorXd tmp(7);
  fill_jacobian(jacobian);
  error = KDL::svd_eigen_HH(jacobian, u_matrix, s_vector, v_matrix, tmp);
  ASSERT_EQ(0, error);
}

TEST (MySvdTest, ReturnsZero) {
  int error;
  Eigen::MatrixXd jacobian(6, 7);
  Eigen::MatrixXd u_matrix(6, 7);
  Eigen::VectorXd s_vector(7);
  Eigen::MatrixXd v_matrix(7, 7);
  Eigen::VectorXd tmp(7);
  fill_jacobian(jacobian);
  error = NEW::my_svd(jacobian, u_matrix, s_vector, v_matrix, tmp);
  ASSERT_EQ(0, error);
}

TEST (EquivalenceTest, EquivalentImplementations) {
  int kdl_error, my_error;
  Eigen::MatrixXd jacobian(6, 7);
  Eigen::MatrixXd my_u(6, 7), kdl_u(6, 7);
  Eigen::VectorXd my_s(7), kdl_s(7);
  Eigen::MatrixXd my_v(7, 7), kdl_v(7, 7);
  Eigen::VectorXd my_tmp(7), kdl_tmp(7);
  float epsilon = 1e-30;

  for (int i=0; i<10000; i++) {
    fill_random_jacobian(jacobian);
    my_error = NEW::my_svd(jacobian, my_u, my_s, my_v, my_tmp);
    kdl_error = KDL::svd_eigen_HH(jacobian, kdl_u, kdl_s, kdl_v, kdl_tmp);
    ASSERT_EQ(my_error, kdl_error);
    ASSERT_EQ(true, similar_matrix(my_u, kdl_u, epsilon));
    ASSERT_EQ(true, similar_matrix(my_v, kdl_v, epsilon));
    ASSERT_EQ(true, similar_vector(my_s, kdl_s, epsilon));
  }
}



int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
