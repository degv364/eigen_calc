#include <gtest/gtest.h>
#include <iostream>
#include "my_svd.hpp"
#include "utilities.hpp"



TEST (StepTest, CorrectSteps) {
    int error;
    double anorm = 0;
    Eigen::MatrixXd jacobian(6, 7);
    Eigen::MatrixXd u_matrix(6, 7);
    Eigen::VectorXd s_vector(7);
    Eigen::MatrixXd v_matrix(7, 7);
    Eigen::VectorXd tmp(7);

    // Init
    fill_jacobian(jacobian);
    u_matrix.setZero();
    u_matrix.topLeftCorner(jacobian.rows(), jacobian.cols())=jacobian;

    // Householder reduction
    error = NEW::householder_reduction_to_bidiagonal(
	u_matrix, s_vector, v_matrix, tmp, &anorm, 1e-300
	);
    ASSERT_EQ(0, error);

    error = NEW::accumulate_right_hand_transformations(
	u_matrix, s_vector, v_matrix, tmp, 1e-300
	);
    ASSERT_EQ(0, error);

    error = NEW::accumulate_left_hand_transformations(
	u_matrix, s_vector, v_matrix, tmp, 1e-300
	);
    ASSERT_EQ(0, error);

    error = NEW::diagonalize_bidiagonal_form(
	u_matrix, s_vector, v_matrix, tmp, 150, anorm, 1e-300
	);
    std::cout<<tmp<<std::endl;
    ASSERT_EQ(0, error);
}
