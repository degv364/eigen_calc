LFLAGS = -Wall -pedantic -lgtest -lpthread -lgtest_main
CFLAGS = -Wall -c -pedantic
COVFLAGS = -fprofile-arcs -ftest-coverage -fPIC -O0 
INCLUDES = -I ./include/ -I /usr/include/eigen3/
SRC = ./src
TEST = ./test/test_equivalence.cpp
UTILS = utilities
OLD = svd_eigen_HH
NEW = my_svd

testing: objects $(TEST)
	g++ $(INCLUDES) $(UTILS).o $(OLD).o $(NEW).o $(TEST) -o test_eq $(LFLAGS)
	g++ $(INCLUDES) $(UTILS).o $(NEW).o ./test/test_steps.cpp -o test_step $(LFLAGS)
	./test_eq
	./test_step

objects: $(SRC)/$(NEW).cpp $(SRC)/$(OLD).cpp $(SRC)/$(UTILS).cpp
	g++ $(INCLUDES) $(CFLAGS) $(SRC)/$(NEW).cpp
	g++ $(INCLUDES) $(CFLAGS) $(SRC)/$(OLD).cpp
	g++ $(INCLUDES) $(CFLAGS) $(SRC)/$(UTILS).cpp

coverage:
	g++ $(COVFLAGS) $(INCLUDES) $(CFLAGS) $(SRC)/$(NEW).cpp
	g++ $(COVFLAGS) $(INCLUDES) $(CFLAGS) $(SRC)/$(OLD).cpp
	g++ $(COVFLAGS) $(INCLUDES) $(CFLAGS) $(SRC)/$(UTILS).cpp
	g++ $(COVFLAGS) $(INCLUDES) $(UTILS).o $(OLD).o $(NEW).o $(TEST) -o test_eq $(LFLAGS)
	./test_eq
	gcovr

clean:
	rm -f *.o *.gcda *gcno test_eq test_step
